package datamanager.logger;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.spi.LoggingEvent;
import org.apache.log4j.spi.ThrowableInformation;

/**
 * Created by jiexa on 14.02.17.
 */
public class MailAppender extends AppenderSkeleton {

  private Properties props;
  private Session session;



  @Override
  protected void append(LoggingEvent loggingEvent) {
    String inputText = layout.format(loggingEvent);

    try {
      setMailConfiguration();
    } catch (IOException e) {
      e.printStackTrace();
    }

    String sender = "malyshevalexey8@gmail.com";
    String recipient = "malyshevalexey8@gmail.com";
    String subject = "Message from java app";
    Message message = new MimeMessage(session);



    try {
      message.setFrom(new InternetAddress(sender));
      message.setRecipients(Message.RecipientType.TO,
          InternetAddress.parse(recipient));
      message.setSubject(subject);
      message.setText(inputText + " \n\n" + loggingEvent.getThrowableInformation().getThrowable()
                                                                  + getStackTrace(loggingEvent));
      Transport.send(message);
    } catch (AddressException e) {
      e.printStackTrace();
    } catch (MessagingException e) {
      e.printStackTrace();
    }





    System.out.println("\nA letter with stacktrace was sent"
                    + "\nCheck " + recipient);

  }

  private void setMailConfiguration() throws IOException {
    String propFilePath = "/home/jiexa/Documents/mail.properties";
    InputStream input = new FileInputStream(propFilePath);

    props = new Properties();
    props.load(input);

    String username = props.getProperty("username");
    String password = props.getProperty("password");

    props.put("mail.smtp.host", "smtp.gmail.com");
    props.put("mail.smtp.socketFactory.port", "465");
    props.put("mail.smtp.socketFactory.class",
        "javax.net.ssl.SSLSocketFactory");
    props.put("mail.smtp.auth", "true");
    props.put("mail.smtp.port", "465");

    session = Session.getInstance(props,
        new javax.mail.Authenticator() {
          protected PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(username, password);
          }
        });

  }

  private String getStackTrace(LoggingEvent loggingEvent) {
    ThrowableInformation throwableInformation = loggingEvent.getThrowableInformation();
    StackTraceElement[] stackTraceArray = throwableInformation.getThrowable().getStackTrace();
    StringBuilder sb = new StringBuilder();
    for (StackTraceElement ste : stackTraceArray) {
      sb.append(ste);
      sb.append("\n");
    }
    return sb.toString();
  }

  public void close() {

  }

  public boolean requiresLayout() {
    return false;
  }
}

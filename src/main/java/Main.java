import CarShop.CarNotFoundException;
import CarShop.Store;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

/**
 * Created by sa on 08.02.17.
 */
public class Main {
    public static final Logger logger = Logger.getLogger(Main.class);


    static {
        DOMConfigurator.configure("src/main/resources/log4j.xml");
    }

    public static void main(String[] args)  {


        Store store = new Store();
        store.createCar(500000, "kia-rio",
                "B146AA");
        String model = "kia-reno";
        try {
            store.sellCar(model,
                    "John",
                    "Konner" ,
                    "+79126241898");
        } catch (CarNotFoundException e) {
            logger.error("Car (model:" + model + ") not found", e);
        }


//        store.save();
    }
}

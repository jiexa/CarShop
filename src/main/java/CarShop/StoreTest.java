package CarShop;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;
import jdk.nashorn.internal.ir.annotations.Ignore;
import models.Car;
import models.Order;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;


/**
 * Created by jiexa on 13.02.17.
 */
class StoreTest {

  public static Store store;

  @BeforeAll
  public static void initStore() {
    store = new Store();
    assertNotNull(store);
  }

  @Test
  void createCar() {
//    Car car = new Car(100, "Nissan", "5454h");
    store.createCar(100, "Nissan", "5454h");

   List<Car> result = store.getFreeCars();
    assertEquals(result.size(), 1);
    result.stream().forEach((car1) ->
    {
      assertEquals(100, car1.getPrice() );
      assertEquals("Nissan", car1.getModel() );
      assertEquals("5454h", car1.getRegNum() );
    });
  }

  @Test
  void save() {

  }

  @Test
  void recover() {

  }

  @Ignore
  @Test
  void getFirstOrder() {
    Store store = new Store();
    store.getOrders();
  }

  @Test
  void sellCar() throws CarNotFoundException {
    Store store = new Store();
    assertThrows(CarNotFoundException.class, () -> store.sellCar("Nissan", "John", "Gold", "+79876543210"));

    store.createCar(100, "Tayota", "565T3W");

      store.sellCar("Tayota","John", "Gold", "+79876543210");

      assertTrue(store.getFreeCars().size() == 0);

     store.getOrders().stream().filter( (order) -> order.getCar().getModel().equals("Tayota")
                                      && order.getCar().getPrice() == 100
                                      && order.getCar().getRegNum().equals("565T3W") );

     assertTrue(store.getContractList().size() == 1);

     assertTrue(store.getContractList().values().stream().filter(
         (client) -> client.getFirstName().equals("John") &&
                     client.getLastName().equals("Gold") &&
                      client.getPhoneNumber().equals("+79876543210")).count()  == 1 );
  }

  @org.junit.jupiter.api.Test
  void getOrders() {

  }

  @Test
  void getContractList() {
    Store store = new Store();
    assertNotNull(store.getContractList());
    assertTrue(store.getContractList().size() > 0);
  }

  @org.junit.jupiter.api.Test
  void getFreeCars() {

  }

}
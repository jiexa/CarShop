package CarShop;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * Created by jiexa on 14.02.17.
 */
public class Customer {

  private static Logger logger = Logger.getLogger(Customer.class);
  static {
    PropertyConfigurator.configure("src/main/resources/log4j.properties");
  }

  private String firstName;
  private String lastName;
  private String phoneNumber;

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }
}

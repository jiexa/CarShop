package CarShop;

import datamanager.DataManager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import models.Car;
import models.Client;
import models.Order;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 * Created by sa on 08.02.17.
 */
public class Store {




  private HashMap<Order, Client> contractList = new HashMap<Order, Client>(256);
  private HashSet<Car> cars = new HashSet<Car>(32);
  private HashSet<Client> clients = new HashSet<Client>(256);
  private static final String FILE_CONTRACTS = "fileContracts.txt";
  private static final String FILE_CARS = "fileCars.txt";
  private static final String FILE_CLIENTS = "fileClients.txt";


  public void createCar(int price, String model,
      String regNumber) {
    Car car = new Car(price, model, regNumber);
    cars.add(car);
  }

  public void save() {
    DataManager.serialize(cars, FILE_CARS);
    DataManager.serialize(clients, FILE_CLIENTS);
    DataManager.serialize(contractList, FILE_CONTRACTS);
  }

  public void recover() {
    ArrayList<Car> list = new ArrayList<Car>();
    DataManager.deserialize(FILE_CARS, list);
    for (Car car :
        list) {
      cars.add(car);
    }

    ArrayList<Client> listClient = new ArrayList<Client>();
    DataManager.deserialize(FILE_CLIENTS, listClient);
    for (Client client :
        listClient) {
      clients.add(client);
    }

    ArrayList<Order> contractListOne = new ArrayList<Order>();
    ArrayList<Client> contractListTwo = new ArrayList<Client>();
    DataManager.deserialize(FILE_CONTRACTS, contractList);
  }

  public Order getFirstOrder() {
    for (Order order :
        contractList.keySet()) {
      return order;
    }
    return null;

  }

  public void sellCar(String model,
      String firstName,
      String lastName,
      String phoneNumber) throws CarNotFoundException {
    Client client = new Client(firstName,
        lastName, phoneNumber);
    clients.add(client);

    Car tmpCar = null;
    for (Car car :
        cars) {
      if (car.getModel().equals(model)) {
        tmpCar = car;
        break;
      }
    }
    if (tmpCar != null) {
      Random random = new Random();
      Order order = new Order(tmpCar,
          tmpCar.getPrice() * 2,
          random.nextLong(), (short) 80
      );
      contractList.put(order, client);
      cars.remove(tmpCar);
    } else {
      CarNotFoundException cnfe = new CarNotFoundException();
//      logger.error("Car with model " + model + " not found" );
      throw cnfe;
    }
  }

  public Set<Order> getOrders() {
    for (Order order :
        contractList.keySet()) {
      System.out.println(order.toString());
    }
    return contractList.keySet();
  }

  public List<Car> getFreeCars() {
    List<Car> list = new ArrayList<Car>();
    for (Car car :
        cars) {
      System.out.println(car.getModel());
      list.add(car);
    }
    return list;
  }

  public Map<Order, Client> getContractList() {
    return contractList;
  }
}
